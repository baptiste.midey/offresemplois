<?php

use App\Http\Controllers\BackendController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OffreController;
use App\Models\Entreprise;
use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AptitudeController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\EleveController;
use App\Http\Controllers\PromotionEleveController;
use App\Http\Controllers\EntretienController;
use App\Http\Controllers\ProfesseurController;
use App\Http\Controllers\FicheController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[OffreController::class, 'welcome'])->name('welcome');
Route::get('/mentions',[BackendController::class, 'mentions'])->name('mentions');
Route::middleware(['auth'])->group(function () {
    Route::get('fiche/pdf', [FicheController::class, 'testViewPDF'])->name('testViewPDF');
    Route::get('fiche/export/{id}', [FicheController::class, 'getPostPdf'])->name('getPostPdf');
    Route::get('/', [OffreController::class, 'welcome'])->name('welcome');
    Route::post('/download', [BackendController::class, 'download'])->name('download');
    Route::get('profil', [UserController::class, 'profil'])->name('profil');
    Route::post('profil', [UserController::class, 'profilUpdate'])->name('profilUpdate');
    Route::post('/contact', [BackendController::class ,'contact'])->name('contact');
    Route::prefix('admin')->group(function () {
        Route::resource('aptitudes', AptitudeController::class);
        Route::resource('offres', OffreController::class);
        Route::resource('entreprises', EntrepriseController::class);
        Route::resource('users', UserController::class);
        Route::resource('option', OptionController::class);
        Route::resource('promotion', PromotionController::class);
        Route::resource('eleve', EleveController::class);
        Route::resource('eleve_promotion', PromotionEleveController::class);
        Route::resource('entretien', EntretienController::class);
        Route::resource('professeur', ProfesseurController::class);
        Route::resource('fiche', FicheController::class);
        Route::get('/', [BackendController::class ,'dashboard'])->name('dashboard');
    });
});
require __DIR__.'/auth.php';