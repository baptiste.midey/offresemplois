<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fiche extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'demandeur_emploi',
        'autre_demandeur',
        'formation_projet',
        'connaissance',
        'motivation',
        'niveau_scolaire',
        'experience_pro',
        'experience_pro_text',
        'atouts',
        'difficultes_solutions',
        'entreprise_trouvee',
        'post_conforme',
        'demarche',
        'type_entreprise',
        'secteur_geo',
        'bilan',
        'amenagements',
        'permis',
        'vehicule',
        'entretien_id',
    ];

    public function entretien()
    {
      return $this->belongsTo(Entretien::class);
    }

}
