<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'description',
        'tuteur_id',
    ];

    public function tuteur()
    {
        return $this->belongsTo(Tuteur::class);
    }

    public function aptitudes()
    {
        return $this->belongsToMany(Aptitude::class);
    }

    
}

   