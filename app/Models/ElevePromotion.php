<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElevePromotion extends Model
{
    use HasFactory;
    protected $fillable = ['professeur_id', 'eleve_id', 'promotion_id', 'date_inscription'];

    public function eleve()
    {
      return $this->belongsTo(Eleve::class);
    }

    public function promotion()
    {
      return $this->belongsTo(Promotion::class);
    }
}
