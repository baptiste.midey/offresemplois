<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Eleve extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = ['age', 'date_naissance', 'code_postal', 'ville', 'adresse',
                            'telephone', 'formation_actuelle', 'diplome', 'specialite_lycee',
                            'langue_vivante_1', 'langue_vivante_2', 'option_id'
                          ];

    public function user()
    {
      return $this->morphOne('App\Models\User', 'profil');
    }

    public function option()
    {
      return $this->belongsTo(Option::class);
    }

    public function promotions(){
      return $this->belongsToMany(Promotion::class)->withTimestamps();
    }

    public function entretiens()
    {
      return $this->hasMany(Entretien::class);
    }
}
