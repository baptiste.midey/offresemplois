<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Professeur extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [];

    public function user()
    {
      return $this->morphOne('App\Models\User', 'profil');
    }

    public function promotions()
    {
      return $this->hasMany(Promotion::class);
    }
}
