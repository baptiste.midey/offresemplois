<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuteur extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user() 
    { 
        return $this->morphOne(User::class, 'profil');
    }

    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function offres()
    {
        return $this->hasMany(Tuteur::class);
    }
}
