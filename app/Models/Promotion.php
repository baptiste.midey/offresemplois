<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{


    protected $fillable = ['id','professeur_id', 'libelle'];

    public function professeur()
    {
      return $this->belongsTo(Professeur::class);
    }

    public function eleves()
    {
      return $this->BelongsToMany(Eleve::class)->withTimestamps();
    }

    public function options()
    {
      return $this->hasMany(Option::class);
    }

    use HasFactory;

}
//des eleves - un prof - des options
