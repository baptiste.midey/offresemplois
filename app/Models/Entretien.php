<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entretien extends Model
{
    use HasFactory;

    protected $fillable = ['date_entretien', 'decision', 'eleve_id', 'fiche_id', 'professeur_id', 'eleve_promotion_id'];

    public function eleve()
    {
      return $this->belongsTo(Eleve::class);
    }

    public function professeur()
    {
      return $this->belongsTo(Professeur::class);
    }

    public function fiche()
    {
      return $this->belongsTo(Fiche::class);
    }

    public function promotion_eleve()
    {
      return $this->belongsTo(PromotionEleve::class);
    }
}
