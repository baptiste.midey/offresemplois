<?php

namespace App\Http\Controllers;

use App\Models\{Entretien, Eleve, Professeur, ElevePromotion};
use Illuminate\Http\Request;


class EntretienController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('log')->only('entretiens.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entretiens = Entretien::all();
        return view('Entretiens/entretiens', compact('entretiens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professeurs = Professeur::all();
        $eleves = Eleve::all();
        return view('Entretiens/create', compact('professeurs', 'eleves'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = request()->validate([
        'date_entretien' => 'required',
        'decision' => 'required',
        'eleve_id' => 'required',
        'professeur_id' => 'required',
        'eleve_promotion_id' => 'required',
      ]);
      $entretien = Entretien::create($data);
    return redirect()->route('entretien.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entretien  $entretien
     * @return \Illuminate\Http\Response
     */
    public function show(Entretien $entretien)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entretien  $entretien
     * @return \Illuminate\Http\Response
     */
    public function edit(Entretien $entretien)
    {
        $professeurs = Professeur::all();
        $eleves = Eleve::all();
        return view('Entretiens/edit', compact('professeurs', 'eleves', 'entretien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entretien  $entretien
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entretien $entretien)
    {
      $request->validate([
        'date_entretien' => 'required',
        'decision' => 'required',
        'professeur_id' => 'required',
        'eleve_id' => 'required',
        'eleve_promotion_id' => 'required',
      ]);
      $entretien->update($request->all());
      return redirect()->route('entretien.index')
                      ->with('success', 'Entretien modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entretien  $entretien
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entretien $entretien)
    {
       $entretien->delete();
       return redirect()->route('entretien.index')
                       ->with('success', 'Entretien supprimé');
    }
}
