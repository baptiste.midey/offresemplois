<?php

namespace App\Http\Controllers;

use App\Models\Eleve;
use App\Models\Promotion;
use App\Models\Option;
use App\Models\ElevePromotion;
use App\Models\Entretien;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EleveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $eleves = Eleve::with('user')->with('promotions')->get();
        return view('eleves.index', compact('eleves'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $promotions = Promotion::all();
        return view('eleves.create', compact('promotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $validator = Validator::make($request->all(), [
            'nom' => 'required|max:255',
            'prenom' => 'required|max:255',
            'email' => 'required|unique:users',
            'password' => 'required',
            'date_naissance' => 'required',
            'telephone' => 'numeric',
            'adresse' => 'required',
            'code_postal' => 'numeric',
            'ville' => 'required',
            'formation_actuelle' => 'required',
            'diplome' => 'required',
            'specialite_lycee' => 'required',
            'langue_vivante_1' => 'required',
            'langue_vivante_2' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('eleves.create')->withErrors($validator)->withInput();
        }
        $jourEntretiens = array();
        foreach($request->get("date_entretiens") as $date_entretien){
            if($date_entretien != null){
                $jourEntretiens[] = $date_entretien;
            }
        }
        $choixPromotions = array();
        foreach($request->get("promotions") as $date){
            $choixPromotions[] = $date;
        }
        $test;
        if(sizeof($choixPromotions) != sizeof($jourEntretiens)){
            return redirect()->route('eleve.create')->withErrors("Les liens entre les promotions et les date d'inscriptions sont incorrectes")->withInput();
        }

        $dateJour = Carbon::now();
        $age = $dateJour->diffInYears($request->get("date_naissance"));

        //Création de l'élève
        $eleve = new Eleve;
        $eleve->age = $age;
        $eleve->date_naissance = $request->get("date_naissance");
        $eleve->adresse = $request->get("adresse");
        $eleve->code_postal = $request->get("code_postal");
        $eleve->ville = $request->get("ville");
        $eleve->telephone = $request->get("telephone");
        $eleve->formation_actuelle = $request->get("formation_actuelle");
        $eleve->diplome = $request->get("diplome");
        $eleve->specialite_lycee = $request->get("specialite_lycee");
        $eleve->langue_vivante_1 = $request->get("langue_vivante_1");
        $eleve->langue_vivante_2 = $request->get("langue_vivante_2");
        $eleve->save();

         
        


        
        $index = 0;
        foreach ($request->get("promotions") as $promotions){
            $promotion = Promotion::find($promotions);

            $eleve->promotions()->attach($promotion);

            //Création de l'entretien
            $entretien = new Entretien;
            $entretien->date_entretien = $jourEntretiens[$index];
            $entretien->eleve_id = $eleve->id;
            $entretien->professeur_id = $promotion->professeur_id;
            
            $eps = DB::table('eleve_promotion')
                ->where('eleve_id', $eleve->id)
                ->where('promotion_id', $promotion->id)
                ->get();
            
            foreach($eps as $ep){
                $entretien->eleve_promotion_id = $ep->id;
            }           
            
            $entretien->save();

            $index++;
            

        }

        //Création de User
        $user = new User;
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->isAdmin = false;
        $user->email = $request->get("email");
        $user->password = Hash::make("password");
        $user->profil_type = "App\Models\Eleve";
        $user->profil_id = $eleve->id;
        $user->save();

        //Création de l'entretien
        



        //Création du lien promotion et élève 
        //$promotionEleve = new PromotionEleve;
        //$promotionEleve->promotion_id = $request->get("promotion_id");
        //$promotionEleve->eleve_id = $request->get("id");
        //$promotionEleve->save();

        return redirect()->route('eleve.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function show(Eleve $eleve)
    {
        //
        return view('eleves.show', compact('eleve'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function edit(Eleve $eleve)
    {
        //Envoyer vers  le page de modification
        $promotions = Promotion::all();
        return view('eleves.edit', compact('eleve', 'promotions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Eleve $eleve)
    {
        //
        $validator = Validator::make($request->all(), [
            'nom' => 'required|max:255',
            'prenom' => 'required|max:255',
            'email' => 'required|email',
            'date_naissance' => 'required',
            'telephone' => 'numeric',
            'adresse' => 'required',
            'code_postal' => 'numeric',
            'ville' => 'required',
            'formation_actuelle' => 'required',
            'diplome' => 'required',
            'specialite_lycee' => 'required',
            'langue_vivante_1' => 'required',
            'langue_vivante_2' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('eleve.edit', $eleve)->withErrors($validator)->withInput();
        }

        $dateJour = Carbon::now();
        $age = $dateJour->diffInYears($request->get("date_naissance"));

         //Modification de l'élève
         $eleve = Eleve::find($eleve->id);
         $eleve->age = $age;
         $eleve->date_naissance = $request->get("date_naissance");
         $eleve->adresse = $request->get("adresse");
         $eleve->code_postal = $request->get("code_postal");
         $eleve->ville = $request->get("ville");
         $eleve->telephone = $request->get("telephone");
         $eleve->formation_actuelle = $request->get("formation_actuelle");
         $eleve->diplome = $request->get("diplome");
         $eleve->specialite_lycee = $request->get("specialite_lycee");
         $eleve->langue_vivante_1 = $request->get("langue_vivante_1");
         $eleve->langue_vivante_2 = $request->get("langue_vivante_2");
         $eleve->save();
         $eleve->promotions()->sync($request->input("promotions"));
         
        $entretiens = Entretien::where('eleve_id', $eleve->id)->get();

        
/*
        $entretiens = DB::table('entretiens')
                        ->join('eleve_promotion', 'eleve_promotion.id', '=', 'entretiens.eleve_promotion_id')
                        ->join('promotions', 'promotions.id', '=', 'eleve_promotion.promotion_id')
                        ->where('entretiens.eleve_id', '=', $eleve->id)
                        ->orderBy('promotions.id')->get();
                        */
        
        $elevePromotions = DB::table('eleve_promotion')->where('eleve_id', $eleve->id)->orderBy('promotion_id')->get();

        $jourEntretiens = array();
        foreach($request->get("date_entretiens") as $date_entretien){
            if($date_entretien != null){
                $jourEntretiens[] = $date_entretien;
            }
        }

        $verif = false;
        $index = 0;
        foreach($elevePromotions as $elevePromotion){
            foreach($entretiens as $entretien){
                if($elevePromotion->id == $entretien->eleve_promotion_id){
                    $verif = true;
                    $e = Entretien::find($entretien->id);
                    $e->date_entretien = $jourEntretiens[$index];
                    $e->save();
                }
            }

            if($verif == false){
                //Création de l'entretien
                $entretien = new Entretien; 
                $entretien->date_entretien = $jourEntretiens[$index];
                $entretien->eleve_id = $eleve->id;
                $entretien->eleve_promotion_id = $elevePromotion->id;
                $pro = Promotion::find($elevePromotion->promotion_id);
                $entretien->professeur_id = $pro->professeur_id;
                $entretien->save();
            }
            $index++;
            $verif = false;
        }
        
        
        
        


         

         //Modification de User
        $user = User::find($eleve->user->id);
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->isAdmin = false;
        $user->email = $request->get("email");
        $user->profil_type = "App\Models\Eleve";
        $user->profil_id = $eleve->id;
        $user->save();
        
        
        /*
                if ($verif == false){
                    $eleve->promotions()->attach($choixPromotion);

                    //Création de l'entretien
                    $entretien = new Entretien;
                    
                    $entretien->eleve_id = $eleve->id;
                    $entretien->professeur_id = $promotion->professeur_id;
                    
                    $eps = DB::table('eleve_promotion')
                        ->where('eleve_id', $eleve->id)
                        ->where('promotion_id', $promotion->id)
                        ->get();
                    
                    foreach($eps as $ep){
                        $entretien->eleve_promotion_id = $ep->id;
                    }           
                    
                    $entretien->save();

                    
                }
                */
        

        return redirect()->route('eleve.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Eleve  $eleve
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eleve $eleve)
    {
        User::destroy($eleve->user->id);
        Eleve::destroy($eleve->id);  

        return redirect()->route('eleve.index');
    }
}
