<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fiche;
use PDF;

class FicheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fiches = Fiche::all();

        return view('fiche.index', compact('fiches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fiche.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'demandeur_emploi'      =>  'required',
            'autre_demandeur'       =>  'required',
            'formation_projet'      =>  'required',
            'connaissance'          =>  'required',
            'motivation'            =>  'required',
            'niveau_scolaire'       =>  'required',
            'experience_pro'        =>  'required',
            'experience_pro_text'   =>  'required',
            'atouts'                =>  'required',
            'difficultes_solutions' =>  'required',
            'entreprise_trouvee'    =>  'required',
            'post_conforme'         =>  'required',
            'demarche'              =>  'required',
            'type_entreprise'       =>  'required',
            'secteur_geo'           =>  'required',
            'bilan'                 =>  'required',
            'amenagements'          =>  'required',
            'permis'                =>  'required',
            'vehicule'              =>  'required',
            'entretien_id'          =>  'required'
        ]);

        $fiche = Fiche::create([
            'demandeur_emploi'         =>  $request->demandeur_emploi,
            'autre_demandeur'          =>  $request->autre_demandeur,
            'formation_projet'         =>  $request->formation_projet,
            'connaissance'             =>  $request->connaissance,
            'motivation'               =>  $request->motivation,
            'niveau_scolaire'          =>  $request->niveau_scolaire,
            'experience_pro'           =>  $request->experience_pro,
            'experience_pro_text'      =>  $request->experience_pro_text,
            'atouts'                   =>  $request->atouts,
            'difficultes_solutions'    =>  $request->difficultes_solutions,
            'entreprise_trouvee'       =>  $request->entreprise_trouvee,
            'post_conforme'            =>  $request->post_conforme,
            'demarche'                 =>  $request->demarche,
            'type_entreprise'          =>  $request->type_entreprise,
            'secteur_geo'              =>  $request->secteur_geo,
            'bilan'                    =>  $request->bilan,
            'amenagements'             =>  $request->amenagements,
            'permis'                   =>  $request->permis,
            'vehicule'                 =>  $request->vehicule,
            'entretien_id'             =>  $request->entretien_id,
        ]);

        return redirect()->route('fiche.index')->with('success', 'Votre fiche à bien ete Uploade');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fiche = Fiche::Find($id);
        return json_encode($fiche);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fiche = Fiche::Find($id);
        return view('fiche.edit',compact('fiche'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fiche = Fiche::Find($id);
        $fiche->demandeur_emploi        =   $request->demandeur_emploi;
        $fiche->autre_demandeur         =   $request->autre_demandeur;
        $fiche->formation_projet        =   $request->formation_projet;
        $fiche->connaissance            =   $request->connaissance;
        $fiche->motivation              =   $request->motivation;
        $fiche->niveau_scolaire         =   $request->niveau_scolaire;
        $fiche->experience_pro          =   $request->experience_pro;
        $fiche->experience_pro_text     =   $request->experience_pro_text;
        $fiche->atouts                  =   $request->atouts;
        $fiche->difficultes_solutions   =   $request->difficultes_solutions;
        $fiche->entreprise_trouvee      =   $request->entreprise_trouvee;
        $fiche->post_conforme           =   $request->post_conforme;
        $fiche->demarche                =   $request->demarche;
        $fiche->type_entreprise         =   $request->type_entreprise;
        $fiche->secteur_geo             =   $request->secteur_geo;
        $fiche->bilan                   =   $request->bilan;
        $fiche->amenagements            =   $request->amenagements;
        $fiche->permis                  =   $request->permis;
        $fiche->vehicule                =   $request->vehicule;
        $fiche->entretien_id            =   $request->entretien_id;
        $fiche->save();

        return redirect()->route('fiche.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fiche $fiche)
    {
        $fiche->delete();
        return redirect()->route('fiche.index');

    }
    public function getPostPdf($id)
    {
        $fiche = Fiche::Find($id);
        // L'instance PDF avec la vue resources/views/posts/show.blade.php
        $pdf = PDF::loadView('fiche.pdf', compact('id','fiche'));
        // Lancement du téléchargement du fichier PDF
        return $pdf->download("EleveID".$id.".pdf");
    }

    public function testViewPDF(){
        $fiche = Fiche::Find(1);
        return view('fiche.pdf',compact('fiche'));   
     }

}
