<?php

namespace App\Http\Controllers;

use App\Models\PromotionEleve;
use Illuminate\Http\Request;

class PromotionEleveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PromotionEleve  $promotionEleve
     * @return \Illuminate\Http\Response
     */
    public function show(PromotionEleve $promotionEleve)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PromotionEleve  $promotionEleve
     * @return \Illuminate\Http\Response
     */
    public function edit(PromotionEleve $promotionEleve)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PromotionEleve  $promotionEleve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PromotionEleve $promotionEleve)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PromotionEleve  $promotionEleve
     * @return \Illuminate\Http\Response
     */
    public function destroy(PromotionEleve $promotionEleve)
    {
        //
    }
}
