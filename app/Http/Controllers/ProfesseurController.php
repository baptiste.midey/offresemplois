<?php

namespace App\Http\Controllers;

use App\Models\{Professeur, User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfesseurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professeurs = Professeur::all();
        return view ('professeur/professeurs', compact('professeurs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('professeur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    //création de professeur
        $professeur = new Professeur;
        $professeur->save();
    //creation de User
        $user = new user;
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->email = $request->get("email");
        $user->profil_type = "App\Models\Professeur";
        $user->profil_id = $professeur->id;
        $user->isAdmin = 0;
        $user->password = hash::make("password");
        $user->save();

        return redirect()->route('professeur.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Professeur  $professeur
     * @return \Illuminate\Http\Response
     */
    public function show(Professeur $professeur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Professeur $professeur)
    {
        return view('professeur.edit', compact('professeur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Professeur $professeur)
    {
//dd($professeur);
        $professeur->save();

        $user = User::find($professeur->user->id);
        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->email = $request->get("email");
        $user->profil_type = "App\Models\Professeur";
        $user->profil_id = $professeur->id;
        $user->isAdmin = 0;
        $user->password = hash::make("password");
        $user->save();

      return redirect()->route('professeur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Professeur $professeur)
    {
        User::destroy($professeur->user->id);
        Professeur::destroy($professeur->id);
        return redirect()->route("professeur.index");
    }
}
