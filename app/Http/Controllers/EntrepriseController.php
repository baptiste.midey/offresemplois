<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entreprise;

class EntrepriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entreprise = Entreprise::latest()->paginate(5);
        return view('entreprises.index',compact('entreprise'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entreprises.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'adresse' => 'required',
            'raison_sociale' => 'required',
            'telephone' => 'required',
            'mail' => 'required',
        ]);
    
        $entreprise = new Entreprise();
        $entreprise->nom=$request->input('nom');
        $entreprise->adresse=$request->input('adresse');
        $entreprise->raison_sociale=$request->input('raison_sociale');
        $entreprise->telephone=$request->input('telephone');
        $entreprise->mail=$request->input('mail');
        $entreprise->save();
        return redirect()->route('entreprises.index')
                        ->with('success','Entreprise créée avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entreprise = Entreprise::find($id);
        return view('entreprises.show')->with('entreprise',$entreprise);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entreprise = Entreprise::find($id);
        return view('entreprises.edit')->with('entreprise',$entreprise);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required',
            'adresse' => 'required',
            'raison_sociale' => 'required',
            'telephone' => 'required',
            'mail' => 'required',
        ]);
        
        $entreprise = Entreprise::find($id);
        $entreprise->nom=$request->input('nom');
        $entreprise->adresse=$request->input('adresse');
        $entreprise->raison_sociale=$request->input('raison_sociale');
        $entreprise->telephone=$request->input('telephone');
        $entreprise->mail=$request->input('mail');
        $entreprise->save();
        return redirect()->route('entreprises.index')
                        ->with('success','Entreprise créée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entreprise = Entreprise::find($id);
        $entreprise->delete();
    
        return redirect()->route('entreprises.index')
                        ->with('success','Entreprise supprimée avec succès');
    }
}
