<?php

namespace App\Http\Controllers;

use App\Models\Offre;
use App\Models\Tuteur;
use App\Models\Aptitude;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offre = Offre::latest()->paginate(5);
        $tuteur = Tuteur::All();
        return view('offres.index',compact('offre'))
            ->with('tuteur', $tuteur);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tuteur = Tuteur::All();
        $aptitude = Aptitude::All();
        return view('offres.create')->with('tuteur', $tuteur)->with('aptitudes', $aptitude);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
        ]);
        $offre = new Offre();
        $offre->nom=$request->input('nom');
        $offre->description=$request->input('description');
        $offre->tuteur_id=$request->input('tuteur');
        if ($request->file('pdf')) {
            $offre->pdf = $request->file('pdf')->store('local');
        }
        $offre->save();
        foreach ($request->get("aptitudes") as $a){
            $offre->aptitudes()->attach($a);
        }
        return redirect()->route('offres.index')
                        ->with('success','Offre créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        return view('offres.show',compact('offre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function edit(Offre $offre)
    {
        $tuteur = Tuteur::All();
        $aptitude = Aptitude::All();
        return view('offres.edit',compact('offre'))->with('offre', $offre)->with('tuteur', $tuteur)->with('aptitudes', $aptitude);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
        ]);
        
        $offre = Offre::find($id);
        $offre->nom=$request->get('nom');
        $offre->description=$request->get('description');
        $offre->tuteur_id=$request->get('tuteur');
        $offre->aptitudes()->sync($request->get("aptitudes"));
        $offre->save();
        return redirect()->route('offres.index')
                        ->with('success','Offre modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offre  $offre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offre $offre)
    {
        $offre->delete();
    
        return redirect()->route('offres.index')
                        ->with('success','Offre supprimé avec succès');
    }

    public function welcome(){
        $offre = Offre::All();
        return view('welcome')
        ->with('offre', $offre);
    }
}
