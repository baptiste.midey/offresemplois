<?php

namespace App\Http\Controllers;

use App\Mail\Mailinfos;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Entreprise;
use App\Models\Tuteur;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::latest()->paginate(5);
    
        return view('users.index',compact('user'))
            ->with('i', (request()->input('page', 1) - 1) * 4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'nom'=> $request->get('nom'),
            'prenom'=> $request->get('prenom'),
            'email'=> $request->get('email'),
            'password'=> $request->get('password'),
        ];

        Validator::make($request->all(), [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'password' => 'required',
            'nom_entreprise' => Rule::requiredIf($request->input('nom_entreprise')),
            'adresse_entreprise' => Rule::requiredIf($request->input('adresse_entreprise')),
            'raison_entreprise' => Rule::requiredIf($request->input('raison_entreprise')),
            'telephone_entreprise' => Rule::requiredIf($request->input('telephone_entreprise')),
            'mail_entreprise' => Rule::requiredIf($request->input('mail_entreprise')),
            'telephone_tuteur' => Rule::requiredIf($request->input('telephone_tuteur')),
        ])->validate();
    
        if ($request->input('nom_entreprise') and $request->input('adresse_entreprise') and $request->input('raison_entreprise') and $request->input('telephone_entreprise') and $request->input('mail_entreprise') != null){

            $entreprise = new Entreprise();
            $entreprise->nom = $request->input('nom_entreprise');
            $entreprise->adresse = $request->input('adresse_entreprise');
            $entreprise->raison_sociale = $request->input('raison_entreprise');
            $entreprise->telephone = $request->input('telephone_entreprise');
            $entreprise->mail = $request->input('mail_entreprise');
            $entreprise->save();
        }

        if ($request->input('telephone_tuteur') != null) {
            $tuteur = new Tuteur();
            $tuteur->telephone = $request->input('telephone_tuteur');
            $tuteur->entreprise_id = $entreprise->id;
            $tuteur->save();
        }

        $user = new User();
        $user->nom=$request->input('nom');
        $user->prenom=$request->input('prenom');
        $user->email=$request->input('email');
        $user->password=bcrypt($request->input('password'));
        if ($request->input('telephone_tuteur') != null){
            $user->profil_type = 'App\Models\Tuteur';
            $user->profil_id = $tuteur->id;
        }
        $user->isAdmin=false;
        $user->save();
        Mail::to('guinguintoto917@gmail.com')->send(new Mailinfos($data));
        return redirect()->route('users.index')->with('success','Utilisateur créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        $user=User::find($id);
        $user->nom=$request->get('nom');
        $user->prenom=$request->get('prenom');
        $user->email=$request->get('email');
        $user->password=bcrypt($request->get('password'));
        $user->isAdmin=false;
        $user->save();
        return redirect()->route('users.index')
                        ->with('success','Utilisateur créé avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    
        return redirect()->route('users.index')
                        ->with('success','Utilisateur supprimé avec succès');
    }

    public function profil(){
        $user = auth()->user();
        return view('users.profil', compact('user'));
    }

    public function profilUpdate(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed'
        ]);

        $user = auth()->user();
        $user->nom = $request->get('nom');
        $user->prenom = $request->get('prenom');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->isAdmin = false;
        $user->save();
        return redirect()->route('profil')
        ->with('success', 'Utilisateur créé avec succès.');
    }
}
