<?php

namespace App\Http\Controllers;

use App\Mail\DemandeContact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BackendController extends Controller
{
    public function dashboard(){
        return view("backend/template");
    }

    public function contact(Request $request){

        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'prenom' => 'required',
            'mail' => 'required',
            'tel' => 'required',
            'objet' => 'required',
            'message' => 'required',
        ]);
        if($validator->fails()) {
            return redirect(url()->previous() . '#contact')
            ->withErrors($validator);
        }

        $data = [
            'nom'=> $request->get('nom'),
            'prenom'=> $request->get('prenom'),
            'tel'=> $request->get('tel'),
            'mail'=> $request->get('mail'),
            'objet'=> $request->get('objet'),
            'message'=> $request->get('message')
        ];

        Mail::to('baptiste.midey@gmail.com')->send(new DemandeContact($data));
        return redirect()->back();
    }

    public function mentions(){
        return view('mentionslegales');
    }

    public function download(Request $request){
        $request->validate(['nom'=>'required']);

        $file = Storage::disk('local')->get($request->input('nom'));

        return (new Response($file, 200))
            ->header('Content-Type', 'application/pdf');
    }
}
