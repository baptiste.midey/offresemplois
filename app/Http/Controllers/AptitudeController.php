<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aptitude;
use App\Models\Offre;

class AptitudeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aptitude = Aptitude::latest()->paginate(7);
        $offre = Offre::All();
        return view('aptitudes.index',compact('aptitude'))
            ->with('i', (request()->input('page', 1) - 1) * 5)->with('aptitude', $aptitude)->with('offre', $offre);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offre = Offre::All();
        return view('aptitudes.create')->with('offre', $offre);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
        ]);
    
        $aptitude = new Aptitude();
        $aptitude->nom=$request->input('nom');
        $aptitude->save();
        return redirect()->route('aptitudes.index')
                        ->with('success','Offre créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Aptitude $aptitude)
    {
        return view('aptitudes.show', compact('aptitude'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Aptitude $aptitude)
    {
        $offre = Offre::All();
        return view('aptitudes.edit',compact('aptitude'))->with('aptitude', $aptitude)->with('offre', $offre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required',
        ]);
        
        $aptitude = Aptitude::find($id);
        $aptitude->nom=$request->get('nom');
        $aptitude->save();
        return redirect()->route('aptitudes.index')
                        ->with('success','Aptitude modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aptitude $aptitude)
    {
        $aptitude->delete();
        return redirect()->route('aptitudes.index')
                        ->with('success','Aptitude supprimé avec succès');
    }
}
