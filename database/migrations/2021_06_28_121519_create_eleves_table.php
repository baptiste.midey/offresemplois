<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElevesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('age');
            $table->date('date_naissance');
            $table->integer('code_postal');
            $table->string('ville');
            $table->string('adresse');
            $table->string('telephone');
            $table->string('formation_actuelle');
            $table->string('diplome');
            $table->string('specialite_lycee');
            $table->string('langue_vivante_1');
            $table->string('langue_vivante_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleves');
    }
}
