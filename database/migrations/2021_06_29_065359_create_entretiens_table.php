<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntretiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entretiens', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_entretien');
            $table->boolean('decision')->nullable();
            $table->unsignedInteger('professeur_id');
            $table->foreign('professeur_id')->references('id')->on('professeurs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedInteger('eleve_id')->nullable();
            $table->foreign('eleve_id')->references('id')->on('eleves')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedInteger('eleve_promotion_id');
            $table->foreign('eleve_promotion_id')->references('id')->on('eleve_promotion')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entretiens');
    }
}
