<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fiches', function (Blueprint $table) {
        
            $table->increments('id');
            $table->integer('demandeur_emploi');
            $table->string('autre_demandeur');
            $table->integer('formation_projet');
            $table->integer('connaissance');
            $table->integer('motivation');
            $table->integer('niveau_scolaire');
            $table->integer('experience_pro');
            $table->string('experience_pro_text');
            $table->string('atouts');
            $table->string('difficultes_solutions');
            $table->integer('entreprise_trouvee');
            $table->integer('post_conforme')->nullable();
            $table->integer('demarche');
            $table->string('type_entreprise');
            $table->string('secteur_geo');
            $table->string('bilan');
            $table->string('amenagements');
            $table->integer('permis');
            $table->integer('vehicule');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fiches');
    }
}
