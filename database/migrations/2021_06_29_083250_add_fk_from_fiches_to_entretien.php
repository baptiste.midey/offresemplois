<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkFromFichesToEntretien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fiches', function (Blueprint $table) {
          $table->unsignedInteger('entretien_id')->nullable();
          $table->foreign('entretien_id')->references('id')->on('entretiens')
              ->onDelete('cascade')
              ->onUpdate('cascade')->nullable();
            $table->unsignedInteger('option_id')->nullable();
            $table->foreign('option_id')->references('id')->on('options')
                  ->onDelete('cascade')
                  ->onUpdate('cascade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fiches', function (Blueprint $table) {
            //
        });
    }
}
