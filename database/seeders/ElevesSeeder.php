<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ElevesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('eleves')->insert([[
            'age'=> 21,
            'date_naissance' => Carbon::create('2000', '01', '01'),
            'adresse' => Str::random(20),
            'code_postal' => 21200,
            'ville' => Str::random(10),
            'telephone' => '06-90-85-52-96',
            'formation_actuelle' => 'Bac général',
            'diplome' => 'Brevet',
            'specialite_lycee' => 'Physique',
            'langue_vivante_1' => 'Anglais',
            'langue_vivante_2' => 'Espagnol'
        ],[
            'age'=> 19,
            'date_naissance' => Carbon::create('2002', '01', '01'),
            'adresse' => Str::random(20),
            'code_postal' => 20100,
            'ville' => Str::random(10),
            'telephone' => '06-90-85-52-96',
            'formation_actuelle' => 'Bac professionnel',
            'diplome' => 'Brevet',
            'specialite_lycee' => 'SN',
            'langue_vivante_1' => 'Anglais',
            'langue_vivante_2' => 'Allemand'
        ]]);
    }
}
