<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PromotionElevesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('eleve_promotion')->insert([[
            'eleve_id' => 1,
            'promotion_id' => 2
            //'date_inscription' => Carbon::create('2021', '06', '01')
        ],[ 
            'eleve_id' => 2,
            'promotion_id' => 1
            //'date_inscription' => Carbon::create('2021', '06', '01')
        ]]);
    }
}
