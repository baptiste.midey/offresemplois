<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AptitudeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aptitudes')->insert([
            'nom' => 'C#',
        ]);
        DB::table('aptitudes')->insert([
            'nom' => 'HTML/CSS',
        ]);
        DB::table('aptitudes')->insert([
            'nom' => 'Symfony',
        ]);
        DB::table('aptitudes')->insert([
            'nom' => 'Linux',
        ]);
        DB::table('aptitudes')->insert([
            'nom' => 'Active Directory',
        ]);
    }
}
