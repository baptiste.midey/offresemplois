<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AptitudeOffreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('aptitude_offre')->insert([
            'offre_id' => '1',
            'aptitude_id' => '4'
        ]);
        DB::table('aptitude_offre')->insert([
            'offre_id' => '1',
            'aptitude_id' => '5'
        ]);
        DB::table('aptitude_offre')->insert([
            'offre_id' => '2',
            'aptitude_id' => '2'
        ]);
        DB::table('aptitude_offre')->insert([
            'offre_id' => '2',
            'aptitude_id' => '3'
        ]);
        DB::table('aptitude_offre')->insert([
            'offre_id' => '3',
            'aptitude_id' => '1'
        ]);
    }
}
