<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;


class EntretiensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('entretiens')->insert([
            'date_entretien' => Carbon::create('2021', '06', '17'),
            'decision' => true,
            'eleve_id' => 1,
            'professeur_id' => 1,
            'eleve_promotion_id' => 1,
            'fiche_id' => null
        ],[
            'date_entretien' => Carbon::create('2021', '07', '17'),
            'decision' => null,
            'eleve_id' => 2,
            'professeur_id' => 2,
            'eleve_promotion_id' => 2,
            'fiche_id' => null
        ]);
    }
}
