<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nom' => 'root',
            'prenom' => 'root',
            'email' => 'root@gmail.com',
            'password' => '$2y$10$HZMuLm8DMUJzBDBtuuSYi.V2sGOeA/SDR7TkRKiMjIH5WfYZNb1Py',
            'isAdmin' => true,
        ]);
        DB::table('users')->insert([
            'nom' => 'tuteurIDMM',
            'prenom' => 'tuteurIDMM', 
            'email' => 'tuteurIDMM@gmail.com',
            'password' => '$2y$10$PkmiOLDhJp1k0U4AIrQJ3eyGcbF7Lb/dSRZOcQHSNykixU6bAw6x2',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '1',
            'isAdmin' => false,
        ]);
        DB::table('users')->insert([
            'nom' => 'eleve',
            'prenom' => 'eleve', 
            'email' => 'eleve@gmail.com',
            'password' => '$2y$10$QUtBQHII9/Pm.ReKH9xKwORYDRu4UE88jAt5C8OrKIajDPBpTcRH6',
            'isAdmin' => false,
        ]);
        DB::table('users')->insert([
            'nom' => 'tuteurPublipress',
            'prenom' => 'tuteurPublipresse', 
            'email' => 'tuteurPublipresse@gmail.com',
            'password' => '$2y$10$85iWh73ZHRpkKlTjI5G4W.nvoNPfl29XCvnTli./a4P2IQ3dHqCBi',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '2',
            'isAdmin' => false,
        ]);
        DB::table('users')->insert([[
            'nom' => 'tuteurDLMSoft',
            'prenom' => 'tuteurDLMSoft', 
            'email' => 'tuteurDLMSoft@gmail.com',
            'password' => '$2y$10$85iWh73ZHRpkKlTjI5G4W.nvoNPfl29XCvnTli./a4P2IQ3dHqCBi',
            'profil_type' => 'App\Models\Tuteur',
            'profil_id' => '3',
            'isAdmin' => false,
        ], [
            'nom' => Str::random(9),
            'prenom' => Str::random(9),
            'isAdmin' => false,
            'profil_type' => 'App\Models\Eleve',
            'profil_id' => 1,
            'email' => Str::random(9) . '@gmail.com',
            'password' => bcrypt('password')
        ], [
            'nom' => Str::random(8),
            'prenom' => Str::random(8),
            'isAdmin' => false,
            'profil_type' => 'App\Models\Eleve',
            'profil_id' => 2,
            'email' => Str::random(8) . '@gmail.com',
            'password' => bcrypt('password')
        ], [
            'nom' => Str::random(7),
            'prenom' => Str::random(7),
            'isAdmin' => false,
            'profil_type' => 'App\Models\Professeur',
            'profil_id' => 1,
            'email' => Str::random(7) . '@gmail.com',
            'password' => bcrypt('password')
        ], [
            'nom' => Str::random(6),
            'prenom' => Str::random(6),
            'isAdmin' => false,
            'profil_type' => 'App\Models\Professeur',
            'profil_id' => 2,
            'email' => Str::random(6) . '@gmail.com',
            'password' => bcrypt('password')
        ]]);

    }
}
