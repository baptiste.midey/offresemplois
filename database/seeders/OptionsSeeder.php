<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('options')->insert([[
            'libelle' => 'SISR',
            'promotion_id' => 2,
        ],[
            'libelle' => 'SLAM',
            'promotion_id' => 2
        ]]);
    }
}
