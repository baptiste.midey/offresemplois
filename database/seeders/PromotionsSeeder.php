<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PromotionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('promotions')->insert([[
            'libelle' => 'ASI',
            'professeur_id' => 1
        ],[
            'libelle' => 'BTS SIO',
            'professeur_id' => 2
        ]]);
    }
}
