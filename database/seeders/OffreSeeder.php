<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OffreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offres')->insert([
            'nom' => 'Recherche apprenti réseau H/F',
            'description' => 'Gérer le parc informatique et les utilisateurs, mis en place de serveurs',
            'tuteur_id' => '1', 
        ]);
        DB::table('offres')->insert([
            'nom' => 'Recherche développeur web H/F',
            'description' => 'Développer des applications pour l\'entreprise et maintenir celles existantes',
            'tuteur_id' => '2', 
        ]);
        DB::table('offres')->insert([
            'nom' => 'Recherche développeur applicatif H/F',
            'description' => 'Développer et mettre à jour les applications logicielles de l\'entreprise',
            'tuteur_id' => '3', 
        ]);
        DB::table('offres')->insert([
            'nom' => 'Recherche apprenti réseau H/F',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsu m has been the industrys standard dummy text ever si of the printing and typesetting industry.',
            'tuteur_id' => '1', 
        ]);
        DB::table('offres')->insert([
            'nom' => 'Recherche développeur web H/F',
            'description' => 'Développer des applications pour l\'entreprise et maintenir celles existantes',
            'tuteur_id' => '2', 
        ]);
        DB::table('offres')->insert([
            'nom' => 'Recherche développeur applicatif H/F',
            'description' => 'Développer et mettre à jour les applications logicielles de l\'entreprise',
            'tuteur_id' => '3', 
        ]);
    }
}
