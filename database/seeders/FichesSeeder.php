<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class FichesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('fiches')->insert([
            'demandeur_emploi' => 2,
            'autre_demandeur' => 'Aucune demande',
            'formation_projet' => 2,
            'connaissance' => 2,
            'motivation' => 3,
            'niveau_scolaire' => 3,
            'experience_pro' => 2,
            'experience_pro_text' => 'Travailler dans un supermarchés',
            'atouts' => 'Réfléchis',
            'difficultes_solutions' => 'Aucune',
            'entreprise_trouvee' => 1,
            'post_conforme' => 1,
            'demarche' => 1,
            'type_entreprise' => 'Entreprise de développement',
            'secteur_geo' => 'Jura',
            'bilan' => 'Bon bilan',
            'amenagements' => 'PAI',
            'permis' => 1,
            'vehicule' => 1,
            'entretien_id' => 1
        ]);

        DB::table('entretiens')->update([
            'id' => 1,
            'fiche_id' => 1
        ]);
    }
}
