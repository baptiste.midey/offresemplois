@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Users</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('users.create') }}"> Créer un utilisateur</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            <th class="action">Action</th>
        </tr>
        @foreach ($user as $u)
        <tr>
           
            <td>{{ $u->nom }}</td>
            <td>{{ $u->prenom }}</td>
            <td>{{ $u->email }}</td>
            <td>
                <form action="{{ route('users.destroy',$u->id) }}" method="POST">
   
                    <a class="btn btn-outline-primary btn-show" href="{{ route('users.show',$u->id) }}"></a>
    
                    <a class="btn btn-outline-success btn-edit" href="{{ route('users.edit',$u->id) }}"></a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
@endsection