@extends('template')
<section class="profil" style="margin-top:150px" >
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="float-start">
                    <h2>Mon profil</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <form action="{{ route('profilUpdate') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <strong>nom</strong>
                        <input type="text" name="nom" value="{{ $user->nom }}" class="form-control"
                            placeholder="Saisir un nom">
                    </div>
                </div>
                <br>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <strong>prenom:</strong>
                        <input type="text" name="prenom" value="{{ $user->prenom }}" class="form-control"
                            placeholder="Saisir un Prenom">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <strong>email:</strong>
                        <input type="text" name="email" id="email" value="{{ $user->email }}" class="form-control"
                            placeholder="Saisir un email" readonly="readonly">
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Mot de passe:</strong>
                            <input type="password" name="password" value="" class="form-control"
                                placeholder="Saisir un mot de passe" required>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <strong>Confirmer le mot de pass:</strong>
                            <input type="password" name="password_confirmation" value="" class="form-control"
                                placeholder="Confirmer le mot de passe" required>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 pt-4">
                        <button type="submit" class="btn btn-outline-primary">Enregistrer</button>
                    </div>
                </div>

        </form>
        <form action="{{ route('users.destroy',$user->id) }}" method="POST">
            @csrf
            @method('DELETE')

            <button type="submit" class="btn btn-outline-danger">Supprimer mon profil</button>
        </form>
    </div>
</section>