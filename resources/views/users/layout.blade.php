<!DOCTYPE html>
<html>
<head>
    <title>CRUD USER APP SCRUM</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('/js/formuser.js')}}"></script>
<style>
body{
    background-color: #FBFEFF;
}
</style>
</head>
<body>
  
<div class="container p-4 mt-5 border border-primary shadow-lg p-3 mb-5 rounded">
    @yield('content')
</div>
   
</body>
</html>