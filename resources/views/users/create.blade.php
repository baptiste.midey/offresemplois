@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Ajouter un utilisateur</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('users.index') }}"> Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('users.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Nom :</strong>
                <input type="text" name="nom" class="form-control" placeholder="Saisir un Nom">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Prénom:</strong>
                <input type="text" name="prenom" class="form-control" placeholder="Saisir un Prénom">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Email :</strong>
                <input type="text" name="email" class="form-control" placeholder="Saisir un Email">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Password :</strong>
                <input type="text" name="password" class="form-control" placeholder="Saisir un mot de passe">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 mt-1">
            <div class="form-group">
                <strong>Ajouter une entreprise :</strong>
                <a class="btn btn-success" role="button" aria-pressed="true" id="btn_entreprise" onclick=entreprise()>+</a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 entreprise" style="display: none">
            <div class="form-group">
                <strong>Nom de l'entreprise :</strong>
                <input type="text" name="nom_entreprise" class="form-control" placeholder="Saisir un nom d'entreprise">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 entreprise" style="display: none">
            <div class="form-group">
                <strong>Adresse de l'entreprise :</strong>
                <input type="text" name="adresse_entreprise" class="form-control" placeholder="Saisir une adresse">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 entreprise" style="display: none">
            <div class="form-group">
                <strong>Raison sociale :</strong>
                <input type="text" name="raison_entreprise" class="form-control" placeholder="Saisir une raison sociale">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 entreprise" style="display: none">
            <div class="form-group">
                <strong>Téléphone entreprise :</strong>
                <input type="text" name="telephone_entreprise" class="form-control" placeholder="Saisir un téléphone">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 entreprise" style="display: none">
            <div class="form-group">
                <strong>Mail entreprise :</strong>
                <input type="text" name="mail_entreprise" class="form-control" placeholder="Saisir un mail">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Type d'utilisateur</strong>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="radio_tuteur" onclick=tuteur()>
                <label class="form-check-label" for="flexRadioDefault1">
                  Tuteur
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="radio_professeur" onclick=tuteur()>
                <label class="form-check-label" for="flexRadioDefault2">
                 Professeur
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="radio_eleve" onclick=tuteur()>
                <label class="form-check-label" for="flexRadioDefault2">
                 Eleve
                </label>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6" id="tuteur" style="display:none;">
            <div class="form-group">
                <strong>Téléphone du tuteur</strong>
                <input type="text" name="telephone_tuteur" class="form-control" placeholder="Saisir un téléphone">
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-2">
        <button type="submit" class="btn btn-primary">Soumettre</button>
    </div>
    </div>
   
</form>
@endsection