@extends('backend.template')

@section('title')
Fiches
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>CRUD Fiche</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-success"  href="{{ route('fiche.create') }}">Ajouter une nouvelle fiche</a>
        </div>
    </div>
  </div>
    
<div class="container">
       <table class="table">
            <thead>
                <th>ID</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($fiches as $fiche)
                    <tr>
                        <td>{{$fiche->id}}</td>
                        <td>
                            <form action="{{ route('fiche.destroy',$fiche->id) }}" method="POST">
                                <a class="btn btn-outline-success btn-edit" href="{{ route('fiche.edit',$fiche->id) }}"></a>
                                <a class="btn btn-outline-sucess btn-export" href="{{ route('getPostPdf',$fiche->id) }}">Export</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
       </table>
</div>
@endsection
