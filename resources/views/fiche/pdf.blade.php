<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <div class="container">
        <div class="r1 c1-6"><h2>Le candidat est il demandeur d'emploi ? </h2>{{ $fiche->demandeur_emploi == 0 ? 'Non' : 'Oui' }}</div>
        <div class="r2 c1-6">
            <h2>Autre :</h2> {{ $fiche->autre_demandeur == 0 ? 'Non' : 'Oui' }}
        </div>
    </div>
    <h2>Elements objectifs receuillis lors de l'entretient de motivation et nécessaires au traitement de la candidature : </h2>
    <div class="container">
        <div class="r1 c1-12">Adéquation entre la formation et le projet : {{ $fiche->formation_projet == 0 ? 'Conforme' : ($fiche->formation_projet == 1 ? 'Non conforme' : 'Autre orientation proposée')  }}</div>
        <div class="r2 c1-6">Connaissance du métier : {{ $fiche->connaissance == 0 ? '++' : ($fiche->connaissance == 1 ? '+' : ($fiche->connaissance == 2 ? '+/-':($fiche->connaissance == 3 ? '-': '--') ))  }}</div>
        <div class="r3 c1-6"> Motivation : {{ $fiche->motivation == 0 ? '++' : ($fiche->motivation == 1 ? '+' : ($fiche->motivation == 2 ? '+/-':($fiche->motivation == 3 ? '-': '--') ))  }}</div>
        <div class="r4 c1-6"> Niveau scolaire : {{ $fiche->niveau_scolaire == 0 ? '<10' : ($fiche->niveau_scolaire == 1 ? '10-12' : ($fiche->niveau_scolaire == 2 ? '12-15':($fiche->niveau_scolaire == 3 ? '>15': 'non concerné') ))  }}</div>
        <div class="r5 c1-12">Experience professionnelle ou associative pouvant être mise en avant auprès d'un futur employeur : {{ $fiche->experience_pro == 0 ? 'Oui significative' : ($fiche->experience_pro == 1 ? 'Oui' : ($fiche->experience_pro == 2 ? 'Non':($fiche->experience_pro == 3 ? '-': '--') ))  }}</div>
        <div class="r6 c1-12">Précision quant à l'experience professionnelle : {{$fiche->experience_pro_text}}</div>
        <div class="r7 c1-12">Atouts : {{$fiche->atouts}}</div>
        <div class="r8 c1-12">Difficulté envisageable et solution possible : {{$fiche->difficultes_solutions}}</div>
    </div>
    <h2>Recherche pour l'entreprise d'accueil : </h2>
    <div class="container">
        <div class="r1 c1-6">Entreprise trouvée : {{ $fiche->formation_projet == 0 ? 'Oui' : 'Non' }}</div>
        <div class="r2 c1-6">Si oui poste conforme à la formation : {{ $fiche->post_conforme == 0 ? 'Oui' : 'Non' }}</div>
        <div class="r3 c1-6">Démarche effectué : {{ $fiche->demarche == 0 ? 'Oui' : 'Non' }}</div>
        <div class="r4 c1-12">Type d'entreprise souhaité : {{$fiche->type_entreprise}}</div>
        <div class="r5 c1-12">Secteurs géographiques de recherche pour l'entreprise d'accueil : {{$fiche->secteur_geo}}</div>
    </div>
    <h2>Bilan : </h2>
    <div class="container">
        <div class="r1 c1-12">{{$fiche->bilan}}</div>
    </div>
    <div class="container">
        <div class="r1 c1-7">
            <div class="container">
                <div class="r1 c1-12"><h2>Aménagement spécifique de la scolarité et/ou de l'examen : </h2></div>
                <div class="r2 c1-12">{{$fiche->amenagements}}</div>
            </div>
        </div>
        <div class="r1 c8-12">
            <div class="container">
                <div class="r1 c1-6">
                    <div class="container">
                        <div class="r1 c1-12"><h2>Permis</h2></div>
                        <div class="r2 c1-12">{{ $fiche->permis == 0 ? 'Oui' : ($fiche->permis == 1 ? 'Non': 'En cours' ) }}</div>
                    </div>
                </div>
                <div class="r1 c6-12">
                    <div class="container">
                        <div class="r1 c1-6"><h2>Véhicule</h2></div>
                        <div class="r2 c1-6">{{ $fiche->vehicule == 0 ? 'Oui' : 'Non' }}</div>
                    </div>
                </div>
                <div class="r2 c1-12 yellow-back"><h2>décision prise à l'égard du candidat</h2></div>
            </div>
        </div>
    </div>
</body>
<style>
body{font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; font-size: 12px;}
h2{
    color :rgb(66, 122, 172);
    font-size: 14px
}
.container{
    display: grid;  
    grid-template-columns:  calc(100% / 12) calc(100% / 12) calc(100% / 12) calc(100% / 12)calc(100% / 12) calc(100% / 12) calc(100% / 12) calc(100% / 12)calc(100% / 12) calc(100% / 12) calc(100% / 12) calc(100% / 12);
    grid-template-rows: auto auto auto auto auto auto auto auto;
}
.r1{grid-row-start: 1;grid-row-end: 1;}
.r2{grid-row-start: 2;grid-row-end: 2;}
.r3{grid-row-start: 3;grid-row-end: 3;}
.r4{grid-row-start: 4;grid-row-end: 4;}
.r5{grid-row-start: 5;grid-row-end: 5;}
.r6{grid-row-start: 6;grid-row-end: 6;}
.r7{grid-row-start: 7;grid-row-end: 7;}
.r8{grid-row-start: 8;grid-row-end: 8;}

.c1-6{grid-column-start: 1;grid-column-end: 6;}
.c1-7{grid-column-start: 1;grid-column-end: 7;}
.c8-12{grid-column-start: 8;grid-column-end: 12;}
.c1-12{ grid-column-start: 1;grid-column-end: 12;}
.c7-12{ grid-column-start: 7;grid-column-end: 12;}

.yellow-back{background-color : rgb(255, 255, 92);}
</style>
</html>
