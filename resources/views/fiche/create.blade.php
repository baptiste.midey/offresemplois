@extends('backend.template')

@section('content')

<form action="{{route('fiche.store')}}" method="POST">
    @csrf
    <div class="field container">
        <div class="row mb-5"> 
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="demandeur_emploi" id="demandeur_emploi">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                    <label for="demandeur_emploi">Demandeur d'emploi</label>
                </div>
            </div>
            <div class="col-9">
                <div class="form-floating">
                    <input class="form-control"type="text" id="autre_demandeur" name="autre_demandeur" placeholder="autre demandeur">
                    <label for="autre_demandeur">Autre demandeur</label>
                </div>
            </div>
        </div>
       
        <div class="row mb-5">
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="formation_projet" name="formation_projet" id="formation_projet">
                        <option value="0">Conforme</option>
                        <option value="1">Non conforme</option>
                        <option value="2">Autre formation proposé</option>
                    </select>
                    <label for="formation_projet">formation_projet</label>
                </div>   
            </div>
            
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="connaissance" id="connaissance">
                        <option value="0">++</option>
                        <option value="1">+</option>
                        <option value="2">+/-</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                    <label for="connaissance">connaissance</label>
                </div>
            </div>
            
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="motivation" id="motivation">
                        <option value="0">++</option>
                        <option value="1">+</option>
                        <option value="2">+/-</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                    <label for="motivation">motivation</label>
                </div>
            </div>
            
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="niveau_scolaire" id="niveau_scolaire">
                        <option value="0"><10</option>
                        <option value="1">10-12</option>
                        <option value="2">12-15</option>
                        <option value="3">>15</option>
                        <option value="4">Non concerné</option>
                    </select>
                    <label for="niveau_scolaire">niveau_scolaire</label>
                </div>
            </div>
        </div>
       
        <div class="row mb-5">
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="experience_pro" id="experience_pro">
                        <option value="0">Oui significative</option>
                        <option value="1">Oui</option>
                        <option value="2">Non</option>
                        <option value="3">-</option>
                        <option value="4">--</option>
                    </select>
                    <label for="experience_pro">Experience pro</label>
                </div>
            </div>
            <div class="col-9">
                <div class="form-floating">
                    <textarea class="form-control"tid="experience_pro_text" name="experience_pro_text" placeholder="Commentaire"></textarea>
                    <label for="experience_pro_text">Commentaire</label>    
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-12">
                <div class="form-floating">
                    <textarea class="form-control"id="atouts" name="atouts" placeholder="atouts"></textarea>
                    <label for="atouts">Atouts</label>    
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-12">
                <div class="form-floating">
                    <textarea class="form-control" id="difficultes_solutions" name="difficultes_solutions" placeholder="difficultes_solutions"></textarea>
                    <label for="difficultes_solutions">Difficulté et solution possible</label>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-6">
                <div class="form-floating">
                    <select class="form-control"  name="entreprise_trouvee" id="entreprise_trouvee">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                    <label for="entreprise_trouvee">Entreprise trouvée</label>
                </div>
            </div>
            <div class="col-6">
                <div class="form-floating">
                    <select class="form-control"  name="post_conforme" id="post_conforme">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                    <label for="post_conforme">Si oui, poste conforme à la formation</label>
                </div>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-6">
                <div class="form-floating">
                    <select class="form-control"  name="demarche" id="demarche">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                    <label for="demarche">Démarche effectutées</label>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-12">
                <div class="form-floating">
                    <input class="form-control"type="text" id="type_entreprise" name="type_entreprise" placeholder="type entreprise">
                    <label for="type_entreprise">Type d'entreprise souhaité</label>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-12">
                <div class="form-floating">
                    <textarea class="form-control" id="secteur_geo" name="secteur_geo" placeholder="secteur_geo"></textarea>
                    <label for="secteur_geo">Sécteur(s) géographique(s) de recherche pour l'entreprise d'accueil</label>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-12">
                <div class="form-floating">
                    <textarea class="form-control" id="bilan" name="bilan" placeholder="bilan"></textarea>
                    <label for="bilan">bilan</label>
                </div>
            </div>
        </div>
       
        <div class="row mb-5">
            <div class="col-6">
                <div class="form-floating">
                    <input class="form-control"type="text" id="amenagements" name="amenagements" placeholder="amenagements" >
                    <label for="amenagements">amenagements</label>
                </div>
            </div>
        </div>
        
        <div class="row mb-5">
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="permis" id="permis">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                        <option value="1">En cours</option>
                    </select> 
                    <label for="permis">permis</label>    
                </div>
            </div>
            <div class="col-3">
                <div class="form-floating">
                    <select class="form-control"  name="vehicule" id="vehicule">
                        <option value="0">Non</option>
                        <option value="1">Oui</option>
                    </select>
                    <label for="vehicule">vehicule</label>
                </div>
            </div>
        </div>
        
        <button type="submit" class="btn">Enregistrer</button>
    </div>

</form>

@endsection