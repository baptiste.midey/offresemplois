@extends('layouts.template')

@section('title')
Fiches
@endsection

@section('content')

<p>Fiches</p>
    
<div class="container">
       <table class="table">
            <thead>
                <th>id</th>
                <th>delete</th>
                <th>update</th>
            </thead>
            <tbody>
                @foreach ($fiches as $fiche)
                    <tr>
                        <td>{{$fiche->id}}</td>
                        <td>
                            <form action="{{route('fiche.destroy', $fiche->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger" type="submit">Delete</button>
                            </form>
                        </td>
                        <td>
                            <a href="{{route('fiche.edit', $fiche->id)}}" name="update" class="btn btn-outline-primary">Modif</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
       </table>
       <div class="">
            <a href="{{route('fiche.create')}}" class="btn btn-primary" type="button">Créer</a>
            <a href="{{route('getPostPdf')}}" class="btn btn-primary" type="button">Export</a>
      </div>
</div>
@endsection
