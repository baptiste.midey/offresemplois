@extends('backend.template')

@section('title')
Options
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>CRUD Option</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-success"  href="{{ route('option.create') }}">Ajouter une nouvelle option</a>
        </div>
    </div>
</div>
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
    <table class="table table-bordered">
        <thead>
            <th>ID</th>
            <th>Libelle</th>
            <th>Promotion</th>
            <th class="action">Action</th>
        </thead>
        <tbody>
            @foreach ($options as $option)
                <tr>
                    <td>{{$option->id}}</td>
                    <td>{{$option->libelle}}</td>
                    <td>{{$option->promotion->libelle}}</td>
                    <td>
                        <form action="{{ route('option.destroy',$option->id) }}" method="POST">
                            <a class="btn btn-outline-success btn-edit" href="{{ route('option.edit',$option->id) }}"></a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection