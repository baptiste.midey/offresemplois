@extends('backend.template')

@section('title')
Créer option
@endsection

@section('content')
<form action="{{route('option.store')}}" method="POST">
    @csrf
    <div class="field">
        <input type="text" name="libelle" id="libelle_create" value="{{old('libelle')}}">
        <select name="promotion_id" id="promotion_id">
            @foreach ($promotions as $promotion)
                <option value="{{$promotion->id}}">{{$promotion->libelle}}</option>
            @endforeach
        </select>
        <button type="submit" class="btn">Creer</button>
    </div>
</form>
@endsection
