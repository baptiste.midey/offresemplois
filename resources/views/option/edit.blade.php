@extends('backend.template')

@section('title')
Créer option
@endsection

@section('content')
<div class="container">
    <form action="{{route('option.update', $option->id)}}" method="POST">
        @csrf
        @method('PUT')
        <input type="text" name="libelle" id="libelle" value="{{$option->libelle}}">
        <select name="promotion_id" id="promotion_id">
            @foreach ($promotions as $promotion)
                <option value="{{$promotion->id}}"{{ $option->promotion_id == $promotion->id ? 'selected' : '' }}>{{$promotion->libelle}}</option>
            @endforeach
        </select>
        <button type="submit" class="btn">Modif</button>
    </form>
</div>
@endsection
