@extends('backend.template')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New professeur</h2>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('professeur.store') }}" method="POST">
    @csrf
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="nom" class="form-control" placeholder="nom" value="{{old('nom')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong>prenom:</strong>
                    <input class="form-control" name="prenom" placeholder="prenom" value="{{old('prenom')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong>Adresse</strong>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse" value="{{old('email')}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <strong for="pass">Password:</strong>
                    <input class="form-control" type="password" id="password" name="password" value="{{old('password')}}">
                </div>
            </div>
            <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection