@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Professeurs</h2>
            </div>
<div class="center">
                <a class="btn btn-outline-success" href="{{ route('professeur.create') }}">Ajouter une nouveau professeur</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<div class="padd20">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>prenom</th>
            <th>nom</th>
            <th>admin</th>
            <th>email</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($professeurs as $professeur)
        <tr>
        <td>{{ $professeur->id }}
            <td>{{ $professeur->user->nom }}</td>
            <td>{{ $professeur->user->prenom }}</td>
            <td>{{ $professeur->user->isAdmin}}</td>
            <td>{{ $professeur->user->email }}</td>
            <td>
            <div class="form-group">
             <form action="{{ route('professeur.destroy',$professeur) }}" method="POST">
             <a class="btn btn-outline-primary btn-edit" href="{{ route('professeur.edit', $professeur) }}"></a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                </form>
            </div>
                    
                <div class="col-xs-12 col-sm-12 col-md-12">
            
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection