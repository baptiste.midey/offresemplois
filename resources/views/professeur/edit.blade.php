@extends('backend.template')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit professeur</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-outline-primary" href="{{ route('professeur.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('professeur.update', $professeur) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom:</strong>
                    <input type="text" name="nom" id="nom" class="form-control" placeholder="Name" value="{{$professeur->user->nom}}">
                </div>
                <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>prenom:</strong>
                    <input type="text" name="prenom" id="prenom" class="form-control" placeholder="prenom" value="{{$professeur->user->prenom}}">
                </div>
                <div class="form-group">
                    <strong>adresse</strong>
                    <input type="text" name="email" id="email" class="form-control" placeholder="adresse" value="{{$professeur->user->email}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection