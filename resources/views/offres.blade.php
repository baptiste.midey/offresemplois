@extends('template')
<section class="offres">
    <div class="container">
        <div class="row">
            <h2>Offres d'emplois</h2>
            @foreach($offre as $o)
            <div class="col-lg-6 ">
                <div class="offre">
                    <p class="nom-entreprise">Publipresse</p>
                    <div class="col-12 description">
                        <h3>{{$o->nom}}</h3>
                        <p>{{$o->description}}</p>
                    </div>
                    <div class="col-12 competence">
                        <h3>compétence demander</h3>
                        <ul>
                            @foreach($o->aptitudes as $a)
                                <li>{{$a->nom}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <button class="btn-postule">Postuler</button>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>