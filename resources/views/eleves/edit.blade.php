@extends('backend.template')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Modifier l'élève : {{$eleve->prenom}} {{$eleve->nom}}</h2>
        </div>
        <div class="float-end">
            <a class="btn btn-outline-primary" href="{{ route('eleve.index') }}"> Retour</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{route('eleve.update', $eleve)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Nom :</strong>
                <input type="text" id="nom" name="nom" value="{{$eleve->user->nom}}" class="form-control" placeholder="Saisir un nom">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Prénom :</strong>
                <input type="text" id="prenom" name="prenom" value="{{$eleve->user->prenom}}" class="form-control" placeholder="Saisir un prénom">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Adresse mail :</strong>
                <input type="text" id="email" name="email" value="{{$eleve->user->email}}" class="form-control" placeholder="Saisir une adresse mail">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Date de naissance:</strong>
                <input type="date" id="date_naissance" value="{{$eleve->date_naissance}}" name="date_naissance" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Téléphone :</strong>
                <input type="text" id="telephone" name="telephone" value="{{$eleve->telephone}}" class="form-control" placeholder="Saisir un numéro de téléphone">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Adresse :</strong>
                <input type="text" id="adresse" name="adresse" value="{{$eleve->adresse}}" class="form-control" placeholder="Saisir une adresse">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Code postal :</strong>
                <input type="text" id="code_postal" name="code_postal" value="{{$eleve->code_postal}}" class="form-control" placeholder="Saisir un code postal">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Ville :</strong>
                <input type="text" id="ville" name="ville" value="{{$eleve->ville}}" class="form-control" placeholder="Saisir une ville">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Formation_actuelle :</strong>
                <input type="text" id="formation_actuelle" name="formation_actuelle" value="{{$eleve->formation_actuelle}}" class="form-control" placeholder="Saisir la formation actuel">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Diplôme :</strong>
                <input type="text" id="diplome" name="diplome" value="{{$eleve->diplome}}" class="form-control" placeholder="Saisir un diplôme">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Spécialiter choisie au  lycée :</strong>
                <input type="text" id="specialite_lycee" name="specialite_lycee" value="{{$eleve->specialite_lycee}}" class="form-control" placeholder="Saisir la spécialiser que l'élève a choisi au lycée">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>LV1:</strong>
                <input type="text" id="langue_vivante_1" name="langue_vivante_1" value="{{$eleve->langue_vivante_1}}" class="form-control" placeholder="Saisir la LV1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>LV2 :</strong>
                <input type="text" id="langue_vivante_2" name="langue_vivante_2" value="{{$eleve->langue_vivante_2}}" class="form-control" placeholder="Saisir la LV2">
            </div>
        </div>
    </div>
    
    <div class="row">
    <label for="promotion">Promotion souhaiter :</label>
        @foreach($promotions as $promotion)
            <div  class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <p>
                        @if($eleve->promotions->contains($promotion->id))
                            <input  type="checkbox" value="{{$promotion->id}}" name="promotions[]" checked="true">
                            <label  for="defaultCheck1">{{$promotion->libelle}}</label>
                            <strong>Date d'entretien :</strong>
                            <input type="date" id="date_entretien[]" name="date_entretiens[]" class="form-control">
                        @else
                            <input  type="checkbox" value="{{$promotion->id}}" name="promotions[]" >
                            <label  for="defaultCheck1">{{$promotion->libelle}}</label>
                            <strong>Date d'entretien :</strong>
                            <input type="date" id="date_entretien[]" name="date_entretiens[]"  class="form-control" disabled>
                        @endif
                    </p>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row">
       <button  class="btn btn-outline-success"> Modifier </button>
    </div>
</form>

<script>
    document.getElementsByName('promotions[]').forEach(element => element.addEventListener('change', function(){
        if(element.checked == true){
            element.parentElement.querySelector('input[name="date_entretiens[]"]').toggleAttribute('disabled', false);
        }
        else{
            element.parentElement.querySelector('input[name="date_entretiens[]"]').toggleAttribute('disabled', true);
        }
    }));
</script>

@endsection

