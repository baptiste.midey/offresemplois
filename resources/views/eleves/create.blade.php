@extends('backend.template')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="center">
            <h2>Ajouter un nouveau élève</h2>
        </div>
        <div class="float-end padd20">
            <a class="btn btn-outline-primary" href="{{ route('eleve.index') }}"> Retour</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups! </strong> Il y a eu des problèmes avec votre entrée.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="padd20">
  <form action="{{ route('eleve.store')}}" method="POST">
      @csrf
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Nom :</strong>
                  <input type="text" id="nom" name="nom" class="form-control" placeholder="Saisir un nom" value="{{old('nom')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Prénom :</strong>
                  <input type="text" id="prenom" name="prenom" class="form-control" placeholder="Saisir un prénom" value="{{old('prenom')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Adresse mail :</strong>
                  <input type="text" id="email" name="email" class="form-control" placeholder="Saisir une adresse mail" value="{{old('email')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Mot de passe :</strong>
                  <input type="password" id="password" name="password" class="form-control" placeholder="Saisir une mot de passe" value="{{old('password')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Date de naissance:</strong>
                  <input type="date" id="date_naissance" name="date_naissance" class="form-control" value="{{old('date_naissance')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Téléphone :</strong>
                  <input type="text" id="telephone" name="telephone" class="form-control" placeholder="Saisir un numéro de téléphone" value="{{old('telephone')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Adresse :</strong>
                  <input type="text" id="adresse" name="adresse" class="form-control" placeholder="Saisir une adresse" value="{{old('adresse')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Code postal :</strong>
                  <input type="text" id="code_postal" name="code_postal" class="form-control" placeholder="Saisir un code postal" value="{{old('code_postal')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Ville :</strong>
                  <input type="text" id="ville" name="ville" class="form-control" placeholder="Saisir une ville" value="{{old('ville')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Formation_actuelle :</strong>
                  <input type="text" id="formation_actuelle" name="formation_actuelle" class="form-control" placeholder="Saisir la formation actuel" value="{{old('formation_actuelle')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Diplôme :</strong>
                  <input type="text" id="diplome" name="diplome" class="form-control" placeholder="Saisir un diplôme" value="{{old('diplome')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>Spécialiter choisie au  lycée :</strong>
                  <input type="text" id="specialite_lycee" name="specialite_lycee" class="form-control" placeholder="Saisir la spécialiser que l'élève a choisi au lycée" value="{{old('specialite_lycee')}}">
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>LV1:</strong>
                  <input type="text" id="langue_vivante_1" name="langue_vivante_1" class="form-control" placeholder="Saisir la LV1" value="{{old('langue_vivante_1')}}">
              </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                  <strong>LV2 :</strong>
                  <input type="text" id="langue_vivante_2" name="langue_vivante_2" class="form-control" placeholder="Saisir la LV2" value="{{old('langue_vivante_2')}}">
              </div>
          </div>
      </div>


      <div class="row">
      <label for="promotion">Promotion souhaiter :</label>
          @foreach($promotions as $promotion)
              <div  class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                      <p>
                          <input type="checkbox" name="promotions[]" id="promotions[]" value="{{$promotion->id}}" /> {{$promotion->libelle}} </br>
                          <strong>Date d'entretien:</strong>
                          <input type="date" id="date_entretiens[]" name="date_entretiens[]" class="form-control" value="{{old('date')}}" disabled>
                      </p>
                  </div>
              </div>
          @endforeach
      </div>

  </form>
  <div class="">
     <button  class="btn btn-outline-success">Valider</button>
  </div>
</div>
<script>
    document.getElementsByName('promotions[]').forEach(element => element.addEventListener('change', function(){
        if(element.checked == true){
            element.parentElement.querySelector('input[name="date_entretiens[]"]').toggleAttribute('disabled', false);
        }
        else{
            element.parentElement.querySelector('input[name="date_entretiens[]"]').toggleAttribute('disabled', true);
        }
    }));
</script>

@endsection
