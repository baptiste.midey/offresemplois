@extends('backend.template')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Eleve</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('eleve.create') }}">Ajouter un nouvel élève</a>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Age</th>
            <th>Promotion voulue</th>
        </tr>

        @foreach($eleves as $eleve)
        <tr>
            <td>{{$eleve->user->nom}}</td>
            <td>{{$eleve->user->prenom}}</td>
            <td>{{$eleve->age}}</td>
            <td>
                @foreach($eleve->promotions as $promotion)
                    {{$promotion->libelle}} /
                @endforeach
            <td>
                <form action="{{ route('eleve.destroy',$eleve->id) }}" method="POST">
                    <a class="btn btn-outline-primary btn-show" href="{{ route('eleve.show',$eleve->id) }}"></a>
                    <a class="btn btn-outline-success btn-edit" href="{{ route('eleve.edit',$eleve->id) }}"></a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  </div>

@endsection
