@extends('template')
{{-- <section>
    <div class="entete">
        <div class="imgfond">
            <img src="{{ url('img/entete.jpg') }}" alt="image entête">
</div>
<h1>Lorem ipsum</h1>
</div>
</section> --}}
<section class="offres">
    <div class="container">
        <div class="row">
            <h2>Offres d'emplois</h2>
            @foreach($offre as $o)
                <div class="col-lg-3 ">
                    <div class="offre">
                        <p class="nom-entreprise">{{ $o->tuteur->entreprise->nom }}</p>
                        <div class="col-12 description">
                            <h3>{{ Str::limit($o->nom, 40 ) }}</h3>
                            <p>{{ Str::limit($o->description, 110) }}</p>
                        </div>
                        {{-- <div class="col-12 competence">
                        <h3>compétence demander</h3>
                        <ul>
@foreach($o->aptitudes as $a)
                                <li>{{ $a->nom }}</li>
            @endforeach
            </ul>
        </div> --}}
        <button class="btn-postule" type="button" data-bs-toggle="modal" data-bs-target="#all{{ $o->id }}">Voir
            plus</button>
        <div class="modal fade" id="all{{ $o->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="staticBackdropLabel">{{ $o->nom }}</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @if($o->pdf)
                            <form method="POST" action="{{ route('download') }}" target="_blank">
                                @csrf
                                <input type="text" value="{{ $o->pdf }}" name="nom" hidden>
                                <button type="submit" class="download">télécharger le pdf</button>
                            </form>
                        @else
                            <p> {{ $o->description }}</p>
                        @endif
                    </div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="staticBackdropLabel">Information entreprise</h4>
                    </div>
                    <div class="modal-body">
                        <h5> Nom entreprise : {{ $o->tuteur->entreprise->nom }}</h5>
                        <p>Nom tuteur : {{ $o->tuteur->user->nom }}</p>
                        <p>Tel : {{ $o->tuteur->telephone }}</p>
                        <p>Email : {{ $o->tuteur->user->email }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-bs-dismiss="modal">fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endforeach
    </div>
    </div>
</section>
<section id="contact">
    <div class="container" id="contact">

        @if($errors->any())
            <div class="alert alert-danger">
                <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('contact') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-6 block">
                    <label for="nom" class="form-label">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" />
                </div>
                <div class="col-md-6 block">
                    <label for="prenom" class="form-label">Prénom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" />
                </div>
                <div class="col-md-6 block">
                    <label for="tel" class="form-label">Téléphone</label>
                    <input type="number" class="form-control" id="tel" name="tel" />
                </div>
                <div class="col-md-6 block">
                    <label for="mail" class="form-label">Mail</label>
                    <input type="text" class="form-control" id="mail" name="mail" />
                </div>
                <div class="col-md-12 block">
                    <label for="objet" class="form-label">Objet</label>
                    <input type="text" class="form-control" id="objet" name="objet" />
                </div>
                <div class="col-md-12 block">
                    <div class="form-label">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" name="message" style="height: 150px"></textarea>
                        <div class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="col-12 btn-contact">
                    <button class="btn btn-lg" type="submit">Envoyez</button>
                </div>
            </div>
        </form>
    </div>
</section>
