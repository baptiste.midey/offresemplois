@extends('backend.template')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Modification de l'entreprise</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour arrière</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('entreprises.update', $entreprise->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom :</strong>
                    <input type="text" name="nom" value="{{ $entreprise->nom }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Adresse :</strong>
                    <textarea class="form-control" style="height:150px" name="adresse" placeholder="Detail">{{ $entreprise->adresse }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Raison sociale : </strong>
                    <input type="text" name="raison_sociale" value="{{ $entreprise->raison_sociale }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Téléphone : </strong>
                    <input type="text" name="telephone" value="{{ $entreprise->telephone }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Mail : </strong>
                    <input type="mail" name="mail" value="{{ $entreprise->mail }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </div>
        </div>
   
    </form>
@endsection