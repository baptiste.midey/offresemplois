@extends('backend.template')
 
@section('content')
  <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Entreprises</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Adresse</th>
            <th>Raison Sociale</th>
            <th>Téléphone</th>
            <th>Mail</th>
            <th class="action">Action</th>
        </tr>
        @foreach ($entreprise as $e)
        <tr>
            <td>{{ $e->id }}</td>
            <td>{{ $e->nom }}</td>
            <td>{{ $e->adresse }}</td>
            <td>{{ $e->raison_sociale }}</td>
            <td>{{ $e->telephone }}</td>
            <td>{{ $e->mail }}</td>
            <td>
                <form action="{{ route('entreprises.destroy',$e->id) }}" method="POST">   
                    <a class="btn btn-outline-primary btn-show" href="{{ route('entreprises.show',$e->id) }}"></a>    
                    <a class="btn btn-outline-success btn-edit" href="{{ route('entreprises.edit',$e->id) }}"></a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>      
@endsection