@extends('backend.template')
  
@section('content')
<script>
    $(document).ready(function(){
        $('input[name="pdf"]').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById('pdf').innerHTML = fileName;
        });
    });
</script>
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Ajouter une nouvelle offre</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oups !</strong> Il y a plusieurs problèmes avec vos saisies.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('offres.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom :</strong>
                <input type="text" name="nom" class="form-control" placeholder="Entrez un nom">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description :</strong>
                <textarea class="form-control" style="height:150px" name="description" placeholder="Entrez une description"></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tuteur : </strong>
                <select name="tuteur" id="tuteur" class="form-control">
                        @foreach($tuteur as $t)
                            <option value="{{ $t->id }}">{{ $t->user->nom }}</option>
                        @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Pdf : </strong>
                <div class="custom-file">
                  <input accept='application/pdf' id="pdf" name="pdf" type="file" class="custom-file-input" lang="fr" required>
                  <label class="custom-file-label" for="pdf"></label>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Aptitude : </strong>
            @foreach($aptitudes as $a)
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]">
                <label class="form-check-label" for="defaultCheck1">
                    {{$a->nom}}
                </label>
            </div>
            @endforeach
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </div>
   
</form>
@endsection

{{-- @section('script')
<script>
    $('#add_app').on('click', function(){
        html = ' <select name="aptitude_id" id="aptitude_id" class="form-control">';
        html += ' <option value="0"></option>';
        html +='                @foreach($aptitudes as $a)';
        html +='                    <option value="{{ $a->id }}">{{ $a->nom }}</option>';
        html +='                @endforeach';
        html +='        </select>';

        $(this).before(html);
    });

    $('#aptitude_form').on('change','select[name="aptitude_id"]',function(){
        console.log('test');

        if($(this).val() == 0){
            $(this).remove();
        }
    });
</script>
@endsection --}}
