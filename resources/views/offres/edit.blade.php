@extends('backend.template')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Modification de l'offre</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour arrière</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('offres.update', $offre->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom :</strong>
                    <input type="text" name="nom" value="{{ $offre->nom }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description :</strong>
                    <textarea class="form-control" style="height:150px" name="description" placeholder="Detail">{{ $offre->description }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tuteur : </strong>
                    <select name="tuteur" id="tuteur" class="form-control">
                        @foreach($tuteur as $t)
                            <option value="{{ $t->id }}">{{ $t->user->nom }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <strong>Aptitude : </strong>
                @foreach($aptitudes as $a )
                <div class="form-check">
                    @if($offre->aptitudes->contains($a->id))
                        <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]" checked="true">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$a->nom}}
                        </label>
                    @else
                        <input class="form-check-input" type="checkbox" value="{{$a->id}}" name="aptitudes[]">
                        <label class="form-check-label" for="defaultCheck1">
                            {{$a->nom}}
                        </label>
                    @endif
                </div>
            @endforeach
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-outline-primary">Envoyer</button>
            </div>
        </div>
   
    </form>
@endsection