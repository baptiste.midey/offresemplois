@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Offres</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success"  href="{{ route('offres.create') }}">Ajouter une nouvelle offre</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th>
            <th>Tuteur</th>
            <th>Aptitude</th>
            <th class="action">Action</th>
        </tr>
        @foreach ($offre as $o)
        <tr>
            <td>{{ $o->id }}</td>
            <td>{{ $o->nom }}</td>
            <td>{{ $o->description }}</td>
            <td>{{ $o->tuteur->user->nom }}</td>
            <td>
                @foreach($o->aptitudes as $aptitude)
                {{ $aptitude->nom }} 
                @endforeach
            </td>
            <td>
                <form action="{{ route('offres.destroy',$o->id) }}" method="POST">   
                    <a class="btn btn-outline-primary btn-show" href="{{ route('offres.show',$o->id) }}"></a>    
                    <a class="btn btn-outline-success btn-edit" href="{{ route('offres.edit',$o->id) }}"></a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn btn-outline-danger btn-delete"></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>  
@endsection