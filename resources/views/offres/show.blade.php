@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Montrer l'offre</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-outline-primary" href="{{ route('offres.index') }}">Retour arrière</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom :</strong>
                {{ $offre->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description :</strong>
                {{ $offre->description }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tuteur : </strong>
                {{ $offre->tuteur->user->nom }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Aptitude : </strong>
                @foreach ( $offre->aptitudes as $a)
                    {{$a->nom}},
                @endforeach
            </div>
        </div>
    </div>
@endsection     