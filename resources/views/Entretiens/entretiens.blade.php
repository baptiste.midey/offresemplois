@extends('backend.template')

@section('title')
Entretiens
@endsection

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
      <div class="float-start">
          <h2>CRUD Entretiens</h2>
      </div>
      <div class="float-end">
          <a class="btn btn-outline-success"  href="{{ route('entretien.create') }}">Ajouter une nouvelle entretien</a>
      </div>
  </div>
</div>

<div class="center padd20">
  <table class="table table-bordered ">
  <tr>
    <th>Date d'entretien</th>
    <th>Decision</th>
    <th>Professeur</th>
    <th>Eleve</th>
    <th>Actions</th>
    @foreach($entretiens as $entretien)
    <tr>
      <td>{{$entretien->date_entretien}}</td>
      <td>{{$entretien->decision}}</td>
      <td>{{$entretien->professeur->user->prenom . " " . $entretien->professeur->user->nom}}</td>
      <td>{{$entretien->eleve->user->prenom . " " . $entretien->eleve->user->nom}}</td>
        <td>
          <form action="{{ route('entretien.destroy',$entretien->id) }}" method="POST">
            <a class="btn btn-outline-success btn-edit" href="{{ route('entretien.edit',$entretien->id) }}"></a>
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger btn-delete"></button>
        </form>
      </td>
    </tr>
    @endforeach
</table>
</div>
@endsection
