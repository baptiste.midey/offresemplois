@extends('backend.template')

@section('title')
Créer entretien
@endsection

@section('content')

<form method="POST" action="{{ route('entretien.store') }}">
    @csrf
    <div class="field">
        <div class="" style="padding: 20px;">
          <label for="">Date d'entretien : </label>
          <input type="text" name="date_entretien" value="{{old('date_entretien', $entretien->date_entretien)}}">
        </div>
        <div class="" style="padding: 20px;">
          <label for="">Décision : </label>
          <select name="decision">
            <option value="1">1</option>
            <option value="0">0</option>
          </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Prof : </label>
          <select name="professeur_id">
              @foreach($professeurs as $prof)
                    <option value="{{$prof->id}}">{{ $prof->user->nom . " " . $prof->user->prenom}} </option>
                @endforeach
            </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Eleve : </label>
          <select name="eleve_id">
              @foreach($eleves as $eleve)
                    <option value="{{$eleve->id}}">{{ $eleve->user->nom . " " . $eleve->user->prenom}}</option>
                @endforeach
            </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Eleve_Promotion : </label>
          <select name="eleve_promotion_id">
              @foreach($eleves as $eleve)
                @foreach ($eleve->promotions as $e )
                    <option value="{{$e->id}}">{{ $e->libelle }}</option>
                @endforeach
              @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary float-right">Submit</button>
    </div>
</form>
@endsection
