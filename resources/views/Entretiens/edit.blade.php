@extends('backend.template')

@section('title')
Modifier entretien
@endsection

@section('content')

<form method="POST" action="{{ route('entretien.update', $entretien->id) }}">
    @csrf
    @method('PUT')
    <div class="field">
        <div class="" style="padding: 20px;">
          <label for="">Date d'entretien : </label>
          <input type="text" name="date_entretien" value="{{$entretien->date_entretien}}">
        </div>
        <div class="" style="padding: 20px;">
          <label for="">Décision : </label>
          <select name="decision">
            @if($entretien->decision == 1)
              <option value="1">1</option>
            @elsif($entretien->decision == 0)
              <option value="0">0</option>
            @else
              <option value="0">0</option>
              <option value="1">1</option>
            @endif
          </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Prof : </label>
          <select name="professeur_id">
              @foreach($professeurs as $professeur)
                @if($professeur->id == $entretien->professeur_id)
                  <option value="{{$professeur->id}}" selected="selected">{{$professeur->user->prenom . " " . $professeur->user->nom}}</option>
                @else
                  <option value="{{$professeur->id}}">{{$professeur->user->prenom . " " . $professeur->user->nom}}</option>
                @endif
              @endforeach
            </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Eleve : </label>
          <select name="eleve_id">
              @foreach($eleves as $eleve)
                @if($eleve->id == $entretien->eleve_id)
                  <option value="{{$eleve->id}}" selected="selected">{{ $eleve->user->prenom . " " . $eleve->user->nom}}</option>
                @else
                  <option value="{{$eleve->id}}"> {{ $eleve->user->prenom . " " . $eleve->user->nom}}</option>
                @endif
              @endforeach
            </select>
        </div>
        <div class="select" style="padding: 20px;">
          <label for="">Promotion de l'élève : </label>
          <select name="eleve_promotion_id">
              @foreach($eleves as $eleve)
                @foreach ($eleve->promotions as $e)
                  @if($e->id == $entretien->eleve_promotion_id)
                    <option value="{{$e->id}}" selected="selected">{{ $e->libelle }}</option>
                  @else
                    <option value="{{$e->id}}">{{ $e->libelle }}</option>
                  @endif
                @endforeach
              @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary float-right">Submit</button>
    </div>
</form>
@endsection
