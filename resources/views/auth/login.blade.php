@extends('template')
<div class="container">
    <x-guest-layout>
        <x-auth-card>
            <x-slot name="logo"></x-slot>
    
            <!-- Session Status -->
            <x-auth-session-status class="mb-4" :status="session('status')" />
    
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <div class="col-md-6 offset-md-3 login">
                <div class="card" style="margin-top: 100px;">
                    <h5 class="card-header">Connexion</h5>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <!-- Email Address -->
                            <div>
                                <x-label for="email" :value="__('Email')" />
                
                                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                            </div>
                            <!-- Password -->
                            <div class="mt-4">
                                <x-label for="password" :value="__('Password')" />
                
                                <x-input id="password" class="block mt-1 w-full"
                                                type="password"
                                                name="password"
                                                required autocomplete="current-password" />
                            </div>
                            <x-button class="ml-3 btn-login">
                                {{ __('Log in') }}
                            </x-button>
                        </form>
                    </div>
                </div>
            </div>
        </x-auth-card>
    </x-guest-layout>
</div>
<style>
    label {min-width: 150px;}
    .login .btn-login {    /* text-align: center; */
    padding: 10px 25px;
    width: 25%;
    margin-left: 41.5%;
    /* bottom: 10px; */
    margin-top: 25px;
    color: black !important;
    border-radius: 0.25rem;
    background-color: transparent;
    text-transform: uppercase;
    transition: background-color 350ms linear;}
</style>