<link  rel="stylesheet" href="{{asset('/css/mail.css')}}">
<body>
    <section id="contenu">
        <h3>Informations de votre compte :</h3>
        <div class="nom">Nom : {{ $data['nom'] }}</div>
        <div class="prenom">Prénom : {{ $data['prenom'] }}</div>
        <div class="mail">Email : {{ $data['email'] }}</div>
        <div class="pwd">Mot de passe : {{ $data['password'] }}</div>
    </section>
</body>
{{-- <strong>Nom :</strong> {{ $data['nom'] }}
<strong>Prénom :</strong> {{ $data['prenom'] }}
<strong>Mail :</strong> {{ $data['email'] }}
<strong>Password :</strong> {{ $data['password'] }} --}}