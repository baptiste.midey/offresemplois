@extends('backend.template')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>CRUD Aptitudes</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-success" href="{{ route('aptitudes.create') }}"> Créer une aptitude</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th class="action">Action</th>
        </tr>
        @foreach ($aptitude as $a)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $a->nom }}</td>
            <td>
                <form action="{{ route('aptitudes.destroy',$a->id) }}" method="POST">   
                    <a class="btn-outline-primary btn-show" href="{{ route('aptitudes.show',$a->id) }}"></a>    
                    <a class="btn-outline-success btn-edit" href="{{ route('aptitudes.edit',$a->id) }}"></a>   
                    @csrf
                    @method('DELETE')      
                    <button type="submit" class="btn  btn-outline-danger btn-delete"></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>      
@endsection