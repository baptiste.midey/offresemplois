@extends('backend.template')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2>Modification de l'aptitude</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-outline-primary" href="{{ route('aptitudes.index') }}">Retour arrière</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oups !</strong> Il y a des problèmes avec vos saisies<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('aptitudes.update', $aptitude->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nom :</strong>
                    <input type="text" name="nom" value="{{ $aptitude->nom }}" class="form-control" placeholder="Title">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Envoyer</button>
            </div>
        </div>
   
    </form>
@endsection