@extends('backend.template')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-start">
                <h2> Afficher l'aptitude</h2>
            </div>
            <div class="float-end">
                <a class="btn btn-primary" href="{{ route('aptitudes.index') }}">Retour arrière</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nom :</strong>
                {{ $aptitude->nom }}
            </div>
        </div>
    </div>
@endsection     