@extends('backend.template')

@section('title')
Créer promotion
@endsection

@section('content')
<form method="POST" action="{{ route('promotion.store') }}">
    @csrf
    <div class="field">
        <div class="select">
          <label for="">Prof : </label>
          <select name="professeur_id">
              @foreach($professeurs as $prof)
                    <option value="{{$prof->id}}">{{ $prof->user->nom . " " . $prof->user->prenom}}</option>
                @endforeach
            </select>
        </div>
        <div class="">
          <label for="">Libelle : </label>
          <input type="text" name="libelle" value="{{old('libelle')}}">
        </div>
        <button type="submit" class="btn btn-primary float-right">Submit</button>
    </div>
</form>
@endsection
