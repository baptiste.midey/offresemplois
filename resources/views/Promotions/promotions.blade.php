@extends('backend.template')

@section('title')
Promotions
@endsection

@section('content')

<div class="row">
  <div class="col-lg-12 margin-tb">
      <div class="float-start">
          <h2>CRUD Promotion</h2>
      </div>
      <div class="float-end">
          <a class="btn btn-outline-success"  href="{{ route('promotion.create') }}">Ajouter une nouvelle promotion</a>
      </div>
  </div>
</div>

<div class="center padd20">
  <table class="table table-bordered ">
    <tr>
      <th>Libelle</th>
      <th>Professeur</th>
      <th>Actions</th>
      @foreach($promotions as $promotion)
      <tr>
        <td>{{$promotion->libelle}}</td>
        <td>{{$promotion->professeur->user->nom}} {{$promotion->professeur->user->prenom}}</td>
        <td style="text-align: center;">
              <form action="{{ route('promotion.destroy',$promotion->id) }}" method="POST">
                <a class="btn btn-outline-success btn-edit" href="{{ route('promotion.edit',$promotion->id) }}"></a>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-outline-danger btn-delete"></button>
            </form>
          </td>
        </tr>
    @endforeach
  </table>
</div>


@endsection
