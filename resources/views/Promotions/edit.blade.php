@extends('backend.template')

@section('title')
Modification promotion
@endsection

@section('content')
<div class="container mt-5">
    <form action="{{ route('promotion.update', $promotion->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label>Libelle</label>
          <input type="text" class="form-control" name="libelle" value="{{ $promotion->libelle }}">
        </div>
        <div class="form-group">
          <label>Professeurs</label>
          <select name="professeur_id">
              @foreach($professeurs as $professeur)
                @if($promotion->professeur_id == $professeur->id)
                  <option value="{{ $professeur->id }}" selected="selected">{{ $professeur->user->nom . " " . $professeur->user->prenom }}</option>
                @else
                  <option value="{{ $professeur->id }}">{{ $professeur->user->nom . " " . $professeur->user->prenom }}</option>
                @endif
              @endforeach
          </select>
        </div>
        <button type="submit" class="btn btn-primary float-right">Submit</button>
    </form>
</div>
@endsection
