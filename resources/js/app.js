require('./bootstrap');

require('alpinejs');

require('./charts');

require('./colors');

require('./main');

require('./popovers');

require('./tooltips');

require('./widgets');